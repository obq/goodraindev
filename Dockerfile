FROM alpine:3.8

ENV SERVER_ADDR 0.0.0.0
ENV SERVER_PORT 443
ENV PASSWORD psw
ENV METHOD      aes-256-cfb
ENV TIMEOUT     300

RUN set -ex \
    && echo "http://mirrors.ustc.edu.cn/alpine/v3.8/main/" > /etc/apk/repositories \
    && apk upgrade \
    && apk add git curl \
	&& apk add --no-cache libsodium python \
    && git clone https://github.com/gitugser/dumpls.git \
	&& apk del --purge git \
    && rm -rf /var/cache/apk/*
	
WORKDIR dumpls/dumpls

EXPOSE 443
#EXPOSE 443 443/udp

CMD python server.py -qq -p $SERVER_PORT -k $PASSWORD -m $METHOD